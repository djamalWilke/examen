<!DOCTYPE html>
    <head>
        <title>poule</title>
        <?php require("imports/head.html"); ?>
    </head>
    <body>
        <?php 
            require("conn.php");
            require("imports/selection.php");
            require("imports/nav.php"); 
            require("imports/isEmpty.php"); 
            if($_SESSION['admin'] == 0) {
                header("location: index.php");
            }
            //heb een id nodig zodat ik weet bij welk pool de uitslag hoort
            if(!isset($_GET['id'])) {
                echo "geen id gevonden";
                die();
            }
            $getid = $_GET['id'];
            
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <?php 
                            $getCountry = "SELECT * FROM `examUser` WHERE `idPoule`= '$getid'";
                            SelectionForm($conn, $getCountry , "idUser", "idUser", "userName");
                    ?>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
            <?php require("imports/scripts.html"); 
            //verwijder een user wanneer die uit de poule moet want bij mij moet je account gekoppelt zijn aan een poule
            if(isset($_POST['submit'])) {
                isEmpty($_POST);
                $idUser = htmlentities(trim($_POST['idUser']), ENT_QUOTES);
                $deleteUser = "DELETE FROM `examUser` WHERE `idUser`=?";
                $stmt = $conn->prepare($deleteUser);
                $stmt->bindParam(1, $idUser);
                $stmt->execute();
            }

            ?>

        </div>
    </body>
</html>