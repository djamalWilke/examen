<nav class="navbar navbar-expand-lg">
  <a class="navbar-brand" href="index.php">ek voetbal</a>
  <button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarNav" aria-controls="navbarNav" aria-expanded="false" aria-label="Toggle navigation">
    <span class="navbar-toggler-icon"></span>
  </button>
  <div class="collapse navbar-collapse" id="navbarNav">
    <ul class="navbar-nav mr-auto">
      <?php if(!empty($_SESSION['idUser'])) { ?>
      <li class="nav-item">
        <a class="nav-link" href="index.php">poule</a>
      </li>
      <?php if( $_SESSION['admin'] == 0) { ?>
      <li class="nav-item">
        <a class="nav-link" href="vote.php">stem</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="updateUser.php">bewerk account</a>
      </li>
      <?php } ?>
      
      <?php if( $_SESSION['admin'] == 1) { ?>
      <li class="nav-item">
        <a class="nav-link" href="country.php">land</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="register.php">gebruiker toevoegen</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="createPoule.php">poule aanmaken</a>
      </li>
      <li class="nav-item">
        <a class="nav-link" href="deletePoule.php">poule verwijderen</a>
      </li>
      <?php } ?>
      <li class="nav-item">
        <a class="nav-link" href="logout.php">log uit</a>
      </li>
      <?php } ?>
      <?php if(empty($_SESSION['idUser'])) { ?>
      <li class="nav-item">
        <a class="nav-link" href="login.php">log in</a>
      </li>
      <?php } ?>

    </ul>
  </div>
</nav>