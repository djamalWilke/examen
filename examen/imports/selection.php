<?php function SelectionForm($conn, $sql , $nameSel, $idName, $displayText, $id = "") { ?>
    <div class="form-group">
            <label for="sel1">Select list:</label>
            <select id="<?= $id ?>" class="form-control select" name="<?= $nameSel ?>">
            <?php
                $stmt = $conn->prepare($sql);
                $stmt->execute();
                if($stmt->rowCount() > 0) {
                    $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($arr as $key => $value) {?>
                        <option value="<?= $value[$idName]; ?>"><?= $value[$displayText]; ?></option>
                        
                    <?php }
                }
            ?>  
            </select>
        </div>
        <?php
}