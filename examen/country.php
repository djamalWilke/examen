<!DOCTYPE html>
    <head>
        <title>poule</title>
        <?php require("imports/head.html"); ?>
    </head>
    <body>
        <?php 
            require("conn.php");
            require("imports/selection.php");
            require("imports/nav.php"); 
            require("imports/isEmpty.php"); 
            if($_SESSION['admin'] == 0) {
                header("location: login.php");
            }
            
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <div class="form-group">
                        <label for="name">Naam</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    <?php 
                    $getPoules = "SELECT * FROM `examPoule`";
                    SelectionForm($conn, $getPoules , "idPoule", "idPoule", "name");
                    ?>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
            <?php require("imports/scripts.html"); 
            //country toegevoegd -> update score voor alle gebruikers met de getoegevoegde landen
            if(isset($_POST['submit'])) {
                isEmpty($_POST);
                $name = htmlentities(trim($_POST['name']), ENT_QUOTES);
                $idPoule = $_POST['idPoule'];
                $createCountrySql = "INSERT INTO `examCountry`(`name`, `idPool`) VALUES ( ?, ?)";
                $stmt = $conn->prepare($createCountrySql);
                $stmt->bindParam(1, $name);
                $stmt->bindParam(2, $idPoule);
                if($stmt->execute()) {
                    echo "gelukt";
                }
            }

            ?>
        </div>
    </body>
</html>