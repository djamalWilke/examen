<!DOCTYPE html>
    <head>
        <title>poule</title>
        <?php require("imports/head.html"); ?>
    </head>
    <body>
        <?php 
            require("conn.php");
            require("imports/nav.php");
            if(empty($_SESSION['idUser'])) {
                header("location: login.php");
            }
            
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                    <?php require("read.php");?>
                </div>
            </div> 
            <?php require("imports/scripts.html"); ?>
    </body>
</html>