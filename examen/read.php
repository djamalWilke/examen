<?php 
//haal de poule id vandaan zodat ik later verschillende tabellen kan laten zien
$readSql = "SELECT * FROM `examPoule` ";
$readSql .= $_SESSION['admin'] == 0 ? "WHERE `idPoule` = (SELECT `idPoule` FROM `examUser` WHERE `idUser` = ?)": "";
$stmt = $conn->prepare($readSql);
if($_SESSION['admin'] == 0) {
    $stmt->bindParam(1, $_SESSION['idUser']);
}

$stmt->execute();
if($stmt->rowCount() > 0) {
    $arr = $stmt->fetchAll(PDO::FETCH_ASSOC);
    foreach ($arr as $key => $resultPoule) {?>
    
    <table class="table table-striped">
        <thead>
            <h3 class="text-center" scope="col"><?= $resultPoule['name'] ?></h3>
            <?php if(!empty($_SESSION['admin'])): ?>
                <a href="countryUpdate.php?id=<?= $resultPoule['idPoule'] ?>"><p>update country</p></a>  
                
                <a href="deleteCountry.php?id=<?= $resultPoule['idPoule'] ?>"><p>delete land van <?= $resultPoule['name'] ?></p></a>    
                <br>
                <a href="userDelete.php?id=<?= $resultPoule['idPoule'] ?>"><p>delete gebruiker van <?= $resultPoule['name'] ?></p></a>
            <?php endif; ?>
            <tr>
                <th scope="col">gebruikersnaam</th>
                <th scope="col">score</th>
            </tr>
        </thead>
        <tbody>
        <?php
            //haal hier de data van voor de tabel om de scoreboord te laten zien
            $readUsersSql = "SELECT * FROM `examUser` WHERE `idPoule` = ? ORDER BY `score` DESC";
            $stmtusers = $conn->prepare($readUsersSql);
            $stmtusers->bindParam(1, $resultPoule['idPoule']);
            $stmtusers->execute();
            if($stmtusers->rowCount() > 0) {
                $users = $stmtusers->fetchAll(PDO::FETCH_ASSOC);
                foreach ($users as $key => $resultUsers) { ?>
                        <tr>
                            <td><?= $resultUsers['userName']; ?></td>
                            <th scope="row"><?= $resultUsers['score']; ?></th>
                        </tr>
                    <?php
                }
            }?>
        </tbody>
    </table>
        <?php
    }
}