
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <?php require("imports/head.html"); ?>
</head>
<body>
<?php 
    require("conn.php");
    require("imports/nav.php");
    require("imports/selection.php"); 
    require("imports/isEmpty.php"); 
    if($_SESSION['admin'] != 1) {
        if(!empty($_SESSION['idUser'])) {
            header("location: index.php");
            die();
        }
        header("location: login.php");
    } 
?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <?php 
                            $getCountry = "SELECT * FROM `examPoule`";
                            SelectionForm($conn, $getCountry , "idPoule", "idPoule", "name");
                    ?>
                    
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
        </div>

    <?php 
    if(isset($_POST['submit'])) {
        isEmpty($_POST);
        //haal eerst de spatie weg daarna maak ik van alle vreemde tekense ascii code van
        $idPoule = htmlentities(trim($_POST['idPoule']), ENT_QUOTES);
        $query = "DELETE FROM `examPoule` WHERE `idPoule`= ?";
        $stmt= $conn->prepare($query);
        $stmt->bindParam(1, $idPoule);
        if($stmt->execute()) {
            header("location: index.php");
        }
    }
    ?>
    <?php require("imports/scripts.html"); ?>
</body>
</html>
<?php

?>