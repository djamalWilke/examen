
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <?php require("imports/head.html"); ?>
</head>
<body>
<?php 
    require("conn.php");
    require("imports/nav.php");
    require("imports/selection.php"); 
    require("imports/isEmpty.php"); 
    if(empty($_SESSION['idUser'])) {
        header("location: login.php");
        die();
    }

?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <div class="form-group">
                        <label for="gebruikersnaam">gebruikersnaam</label>
                        <input type="text" class="form-control" name="gebruikersnaam" id="gebruikersnaam">
                    </div>
                    
                    <div class="form-group">
                        <label for="password">wachtwoord</label>
                        <input type="text" class="form-control" name="password" id="password">
                    </div>
                    <div class="form-group">
                        <label for="email">poule naam</label>
                        <input type="text" class="form-control" name="email" id="email">
                    </div>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
        </div>

    <?php 
    if(isset($_POST['submit'])) {
        //haal eerst de spatie weg daarna maak ik van alle vreemde tekense ascii code van en voor password md5
        $email = htmlentities(trim($_POST['email']), ENT_QUOTES);
        $password = md5(htmlentities(trim($_POST['password']), ENT_QUOTES));
        $gebruikersnaam = htmlentities(trim($_POST['gebruikersnaam']), ENT_QUOTES);
        if(!empty($email) || !empty($password) || !empty($gebruikersnaam)) {
            $count = 0;
            $queryEmail = "";
            $queryPassword = "";
            $queryGebruikersnaam = "";
            $comma = ["", ""];
            if(!empty($email)) {
                $count++;
                $queryEmail =  "`email`= $email ";
            }
            if(!empty($password)) {
                $count++;
                $queryPassword =  "`password`= $password ";
            }
            if(!empty($gebruikersnaam)) {
                $count++;
                $queryGebruikersnaam =  "`userName`= $gebruikersnaam";
            }
            if($count > 1) {
                $comma[0] = ",";
            }
            if($count > 2) {
                $comma[1] = ",";
            }
            $query = "UPDATE `examUser` SET $queryGebruikersnaam" .  $comma[0] ." $queryPassword" .  $comma[1] ." $queryEmail     WHERE `idUser`=?";
            $stmt= $conn->prepare($query);
            $stmt->bindParam(1, $_SESSION['idUser']);
            if($stmt->execute()) {
                echo "gelukt";
            }
        }
        
    }
    ?>
    <?php require("imports/scripts.html"); ?>
</body>
</html>
<?php

?>