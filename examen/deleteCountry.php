
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <?php require("imports/head.html"); ?>
</head>
<body>
<?php 
    require("conn.php");
    require("imports/nav.php");
    require("imports/selection.php"); 
    require("imports/isEmpty.php"); 
    if($_SESSION['admin'] != 1) {
        if(!empty($_SESSION['idUser'])) {
            header("location: index.php");
            die();
        }
        header("location: login.php");
    } 
    if(!isset($_GET['id'])) {
        echo "geen id gevonden";
        die();
    }
    $getid = $_GET['id'];
?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <?php 
                            $getCountry = "SELECT * FROM `examCountry` WHERE `idPool`= $getid";
                            SelectionForm($conn, $getCountry , "idCountry", "idCountry", "name");
                    ?>
                    
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
        </div>

    <?php 
    if(isset($_POST['submit'])) {
        isEmpty($_POST);
        //haal eerst de spatie weg daarna maak ik van alle vreemde tekense ascii code van
        $idCountry = htmlentities(trim($_POST['idCountry']), ENT_QUOTES);
        $query = "DELETE FROM `examCountry` WHERE `idPool`= ? AND `idCountry` = ?";
        $stmt= $conn->prepare($query);
        $stmt->bindParam(1, $getid);
        $stmt->bindParam(2, $idCountry);
        if($stmt->execute()) {
            header("location: index.php");
        }
    }
    ?>
    <?php require("imports/scripts.html"); ?>
</body>
</html>
<?php

?>