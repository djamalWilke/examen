<!DOCTYPE html>
    <head>
        <title>poule</title>
        <?php require("imports/head.html"); ?>
    </head>
    <body>
        <?php 
            require("conn.php");
            require("imports/selection.php");
            require("imports/nav.php"); 
            require("imports/isEmpty.php"); 
            if($_SESSION['admin'] == 1) {
                header("location: index.php");
            }
            
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <?php 
                        for ($i=1; $i <= 4; $i++) { 
                            $getCountry = "SELECT * FROM `examCountry` WHERE `idPool`= (SELECT `idPoule` FROM `examUser` WHERE `idUser`= " . $_SESSION['idUser'] .")";
                            SelectionForm($conn, $getCountry , "idCountry$i", "idCountry", "name", "idCountry$i");
                        }
                    ?>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
            <?php require("imports/scripts.html"); 
            //country toegevoegd -> update score voor alle gebruikers met de getoegevoegde landen
            if(isset($_POST['submit'])) {
                isEmpty($_POST);
                $idCountry = [];
                for ($i=1; $i <= 4; $i++) { 
                    $idCountry[$i] = htmlentities(trim($_POST['idCountry' . $i]), ENT_QUOTES);
                }
                //check of er meerde var in de array het zelfde zijn anders kan die gwn verder
                for ($i=1; $i <= count($idCountry); $i++) {
                    for ($j=1; $j <= count($idCountry); $j++) {
                        if($i != $j) {
                            if($idCountry[$i] == $idCountry[$j]) {
                                echo "duplicaat gevonden";
                                die();
                            }
                        }
                    } 
                }
                //check of de gebruiker al heeft gestemt als dat waar is dan gaat die de al gestemde rows updaten
                $getUserVote = "SELECT `idVote` FROM `examVote` WHERE `idUser`= ?";
                $stmt = $conn->prepare($getUserVote);
                $stmt->bindParam(1, $_SESSION['idUser']);
                $stmt->execute();
                $Updateid = [0];
                $selectCount = $stmt->rowCount();
                if($selectCount == 4) {
                    $dataVote = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    foreach ($dataVote as $key => $resultVote) {
                        $Updateid[] = $resultVote['idVote'];
                    }
                                }
                for ($i=1; $i <= count($idCountry); $i++) { 
                    $updateVote = "INSERT INTO `examVote`(`place`, `idCountry`, `idUser`) VALUES (?, ?, ?)";
                    if( $selectCount == 4) {
                        echo "update";
                        $updateVote = "UPDATE `examVote` SET `place` = ?, `idCountry` = ?, `idUser`= ? WHERE idVote = ?";
                    }
                    $stmt = $conn->prepare($updateVote);
                    $stmt->bindParam(1, $i);
                    $stmt->bindParam(2, $idCountry[$i]);
                    $stmt->bindParam(3, $_SESSION['idUser']);
                    if($selectCount== 4) {
                        $stmt->bindParam(4, $Updateid[$i]);
                    }
                    $stmt->execute();
                }
            }
            ?>
            <script>
                $(document).ready(function(){
                    var arr = [];
                    for (let index = 1; index <= 4; index++) {
                        $("#idCountry" + index).change(function(){
                            console.log("fsfa");          
                            var value = $("#idCountry" + index +" option:selected").val();
                                console.log(index + "    " + value);
                                arr.push(value);
                                console.log([0]);
                            //disbales the selected option
                            console.log('vsavas   ' + $('#idCountry' + index).val());
                            var child = $("#idCountry" + index ).children();
                            for (let index = 0; index < child.length; index++) {
                                if (!$.inArray($(child[index]).val(), arr)) {
                                        console.log("found" + $(this).val());
                                        console.log(arr);
                                        $(this).prop('disabled', true);
                                    } else {

                                        $(this).prop('disabled',false);
                                    }
                            }
                            $("#idCountry" + index ).children().each(function() {
                                    
                            });
                        });   
                    } 
                });
            </script>
        </div>
    </body>
</html>