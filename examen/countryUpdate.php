<!DOCTYPE html>
    <head>
        <title>poule</title>
        <?php require("imports/head.html"); ?>
    </head>
    <body>
        <?php 
            require("conn.php");
            require("imports/selection.php");
            require("imports/nav.php"); 
            require("imports/isEmpty.php"); 
            if($_SESSION['admin'] == 0) {
                header("location: login.php");
            }
            //heb een id nodig zodat ik weet bij welk pool de uitslag hoort
            if(!isset($_GET['id'])) {
                echo "geen id gevonden";
                die();
            }
            $getid = $_GET['id'];
        ?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <div class="form-group">
                        <label for="win">gewonnen</label>
                        <input type="int" min="0" class="form-control" name="win" id="win">
                    </div>
                    <div class="form-group">
                        <label for="tie">gelijk</label>
                        <input type="int" min="0" class="form-control" name="tie" id="tie">
                    </div>
                    <div class="form-group">
                        <label for="losses">verloren</label>
                        <input type="int" min="0" class="form-control" name="losses" id="losses">
                    </div>
                    <?php 
                    $getCountry = "SELECT * FROM `examCountry` WHERE `idPool`= $getid";
                    SelectionForm($conn, $getCountry , "idCountry", "idCountry", "name");
                    ?>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
            <?php require("imports/scripts.html"); 
            //country toegevoegd -> update score voor alle gebruikers omdat misschien verandert hun plek op het boord
            if(isset($_POST['submit'])) {
                isEmpty($_POST);
                $win = htmlentities(trim($_POST['win']), ENT_QUOTES);
                $tie = htmlentities(trim($_POST['tie']), ENT_QUOTES);
                $losses = htmlentities(trim($_POST['losses']), ENT_QUOTES);
                $idCountry = htmlentities(trim($_POST['idCountry']), ENT_QUOTES);
                //hoe je punten eruit moeten zien 1:10, 2:6, 3:4, 4:2
                $changePoule = !empty($changePoule) ? ",`idPool`= :id":"";
                $createCountrySql = "UPDATE `examCountry` SET `wins`=?,`ties`=?,`losses`=? WHERE `idCountry`= ?";
                $stmt = $conn->prepare($createCountrySql);
                $stmt->bindParam(1, $win);
                $stmt->bindParam(2, $tie);
                $stmt->bindParam(3, $losses);
                $stmt->bindParam(4, $idCountry);

                if($stmt->execute()) {
                    //bereken de volgorde van de beste landen
                    $getCountry = "SELECT * FROM `examCountry` WHERE `idPool`= (SELECT `idPool` FROM `examCountry` WHERE `idCountry` = ?)";
                    $stmt = $conn->prepare($getCountry);
                    $stmt->bindParam(1, $idCountry);
                    $stmt->execute();
                    if($stmt->rowCount() > 0) {
                        $countryPoule = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        $scores = [];
                        foreach ($countryPoule as $key => $country) {
                            $id = (int)$country['idCountry'];
                            $scores[$id] = $country['wins'] - $country['losses'] + ($country['ties']);
                        }
                        arsort($scores);
                        $count = 0;
                        $scoreUpdate = [];
                        //heb de plaatsen bereken nu ga ik de score bijhouden van de spelers zodat er bij kan komen
                        foreach ($scores as $keyScore => $score) {
                            $count++;
                            $getUser = "SELECT `idUser`,`score` FROM `examUser` WHERE `admin` = 0";//score + id
                            $stmt = $conn->prepare($getUser);
                            $stmt->execute();
                            if($stmt->rowCount() > 0) {
                                $dataUsers = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                
                                foreach ($dataUsers as $key => $resultUsers) {
                                    //gebruik hier een array omdat die eroverheen loopt en dan weer van de db data begint waardoor je score alleen de laatste keer optelt
                                    if(!isset($scoreUpdate[(int)$resultUsers['idUser']])) {
                                        $scoreUpdate[(int)$resultUsers['idUser']] = $resultUsers['score'];
                                    } 
                                    //gebruik dit om je gekoozen plek te matchen met de juiste uitkomst
                                    $getPlace = "SELECT `place`,`idCountry` FROM `examVote` WHERE `idUser` = ?";//place + country
                                    $stmt = $conn->prepare($getPlace);
                                    $stmt->bindParam(1, $resultUsers['idUser']);
                                    $stmt->execute();
                                    if($stmt->rowCount() > 0) {
                                        $dataPlace = $stmt->fetchAll(PDO::FETCH_ASSOC);
                                        foreach ($dataPlace as $key => $resultPlace) {
                                           if($keyScore != $resultPlace['idCountry']) {
                                                continue;
                                            }

                                            if($count == $resultPlace['place'] && $resultPlace['place'] == 1) {
                                                $scoreUpdate[(int)$resultUsers['idUser']] += 10;
                                            }  else if($count == $resultPlace['place'] && $resultPlace['place'] == 2) {
                                                $scoreUpdate[(int)$resultUsers['idUser']] += 6;
                                            } else if($count == $resultPlace['place'] && $resultPlace['place'] == 3) {
                                                $scoreUpdate[(int)$resultUsers['idUser']] += 4; 
                                            } else if($count == $resultPlace['place'] && $resultPlace['place'] == 4) {
                                                $scoreUpdate[(int)$resultUsers['idUser']] += 2;
                                            } 
                                        }
                                    }
                                }   
                            }
                            
                        }
                    } 
                    // alles klaar dan uploaden
                    foreach ($scoreUpdate as $keyUpdate => $value) {
                        $updateUserScore = "UPDATE `examUser` SET `score`= ? WHERE `idUser`= ?";
                        $stmt = $conn->prepare($updateUserScore);
                        $stmt->bindParam(1, $value);
                        $stmt->bindParam(2, $keyUpdate);
                        $stmt->execute();
                    }
                    //
                    $checkEmail = "SELECT * FROM `examCountry` WHERE `idPool`= ? ";
                    $stmt = $conn->prepare($checkEmail);
                    $stmt->bindParam(1, $getid);
                    $stmt->execute();
                    $countryUpdated = $stmt->fetchAll(PDO::FETCH_ASSOC);
                    $sendMail = true;
                    foreach ($countryUpdated as $key => $countryUpdated) {
                        //kijk of er een waarde is gevult dat betekend dat de land is zijn resultaat heeft dan kan ik een mail gaan sturen
                        if($countryUpdated['wins'] != 0 || $countryUpdated['ties'] != 0 || $countryUpdated['losses'] != 0) {
                            $sendMail = true;
                        } else {
                            $sendMail = false;
                        }
                    }
                    //haal de email adressen op en verstuur de emails als alle landen een uitslag hebben
                    if($sendMail) {
                        $getEmail = "SELECT `email`,`score` FROM `examUser` WHERE `admin` = '0' AND `idPoule`= ?";
                        $stmt = $conn->prepare($getEmail);
                        $stmt->bindParam(1, $getid);
                        $stmt->execute();
                        $userInfo = $stmt->fetchAll(PDO::FETCH_ASSOC);
                        foreach ($userInfo as $key => $userInfo) {
                            $to      = $userInfo['email'];
                            $subject = 'resultaat';
                            $message = "de landen hebben allemaal een resultaat gekregen uw score is" . $userInfo['score'];
                            $headers = 'From: ek-poule@ek-poule.com' . "\r\n" .
                                'Reply-To: ek-poule@ek-poule.c' . "\r\n" .
                                'X-Mailer: PHP/' . phpversion();

                            mail($to, $subject, $message, $headers);
                        }
                    }
                }
            }

            ?>
        </div>
    </body>
</html>