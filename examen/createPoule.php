
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <?php require("imports/head.html"); ?>
</head>
<body>
<?php 
    require("conn.php");
    require("imports/nav.php");
    require("imports/selection.php"); 
    require("imports/isEmpty.php"); 
    if($_SESSION['admin'] != 1) {
        if(!empty($_SESSION['idUser'])) {
            header("location: index.php");
            die();
        }
        header("location: login.php");
    } 
?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <div class="form-group">
                        <label for="name">poule naam</label>
                        <input type="text" class="form-control" name="name" id="name">
                    </div>
                    
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
        </div>

    <?php 
    if(isset($_POST['submit'])) {
        isEmpty($_POST);
        //haal eerst de spatie weg daarna maak ik van alle vreemde tekense ascii code van
        $name = htmlentities(trim($_POST['name']), ENT_QUOTES);
        $query = "INSERT INTO `examPoule`( `name`) VALUES (?)";
        $stmt= $conn->prepare($query);
        $stmt->bindParam(1, $name);
        if($stmt->execute()) {
            echo "gelukt";
        }
    }
    ?>
    <?php require("imports/scripts.html"); ?>
</body>
</html>
<?php

?>