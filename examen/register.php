
<!DOCTYPE html>
<html lang="en">
<head>
    <title>Document</title>
    <?php require("imports/head.html"); ?>
</head>
<body>
<?php 
    require("conn.php");
    require("imports/nav.php");
    require("imports/selection.php"); 
    require("imports/isEmpty.php"); 
    if($_SESSION['admin'] != 1) {
        if(!empty($_SESSION['idUser'])) {
            header("location: index.php");
            die();
        }
        header("location: login.php");
    } 
?>
        <div class="container">
            <div class="row">
                <div class="col-sm">
                <form method="post">
                    <div class="form-group">
                        <label for="username">gebruikersnaam</label>
                        <input type="text" class="form-control" name="username" id="username">
                    </div>
                    <div class="form-group">
                        <label for="password">wachtwoord</label>
                        <input type="text" class="form-control" name="password" id="password">
                    </div>
                    <div class="form-group">
                        <label for="admin">admin</label>
                        <input type="number" min="0" max="1" value="0" class="form-control" name="admin" id="admin">
                    </div>
                    <div class="form-group">
                        <label for="email">email</label>
                        <input type="text" class="form-control" name="email" id="email">
                    </div>
                    <?php 
                    $getPoules = "SELECT * FROM `examPoule`";
                    SelectionForm($conn, $getPoules , "idPoule", "idPoule", "name");
                    ?>
                    <button type="submit" name="submit" class="btn btn-primary">Submit</button>
                    </form>
                </div>
            </div> 
        </div>

    <?php 
    if(isset($_POST['submit'])) {
        isEmpty($_POST);
        //form data
        //haal eerst de spatie weg daarna maak ik van alle vreemde tekense ascii code van en bij het wachtwoord maak ik daarna ook nog een md5
        $username = htmlentities(trim($_POST['username']), ENT_QUOTES);
        $password = md5(htmlentities(trim($_POST['password']), ENT_QUOTES));
        $admin = htmlentities(trim($_POST['admin']), ENT_QUOTES);
        $email = $_POST['email'];
        $score = 0;
        $idPoule = htmlentities(trim($_POST['idPoule']), ENT_QUOTES);

        //mail data
        $to      = $email;
        $subject = 'the subject';
        $message = "uw inlog gegevens kunt u hier onder vinden\r\n gebruikersnaam: $username \r\n wachtwoord: " . htmlentities(trim($_POST['password']), ENT_QUOTES);
        $headers = 'From: ek-poule@ek-poule.com' . "\r\n" .
            'Reply-To: ek-poule@ek-poule.c' . "\r\n" .
            'X-Mailer: PHP/' . phpversion();

        
        if(empty($idPoule)) {
            die();
        }   
        $query = "INSERT INTO `examUser`(`userName`, `password`,`email`, `admin`, `score`, `idPoule`) VALUES (?, ?, ?, ?, ?, ?)";
        $stmt= $conn->prepare($query);
        $stmt->bindParam(1, $username);
        $stmt->bindParam(2, $password);
        $stmt->bindParam(3, $email);
        $stmt->bindParam(4, $admin);
        $stmt->bindParam(5, $score);
        $stmt->bindParam(6, $idPoule);
        if($stmt->execute()) {
            echo "gelukt";
            mail($to, $subject, $message, $headers);
        }
    }
    ?>
    <?php require("imports/scripts.html"); ?>
</body>
</html>
<?php

?>